#include "rawstate.h"

#include "quantum.h"

RawState::RawState() = default;

ket RawState::From(const std::vector<double>& parameters)
{
    assert(parameters.size() % 2 == 0);

    auto d = parameters.size() / 2;
    std::vector<scalar> complex_parameters(d);
    for (auto i = 0; i < d; i ++)
        complex_parameters[i] = parameters[2*i] + J*parameters[2*i + 1];

    Eigen::Map<const ket> state(complex_parameters.data(), d, 1);
    return state.normalized();
}

ket RawState::DFrom(const size_t i, const size_t spins)
{
    const size_t p_count = ParameterCount(spins);
    assert(i < p_count);
    const size_t d = p_count / 2;
    std::vector<scalar> complex_parameters(d, 0.);
    if (i % 2 == 0)
        complex_parameters[i / 2] = 1. + J*0.;
    else
        complex_parameters[i / 2] = 0. + J*1.;

    Eigen::Map<const ket> state(complex_parameters.data(), d, 1);
    return state.normalized();
}

size_t RawState::ParameterCount(const size_t spins)
{
    // one complex vector of 2^spins entries
    return 2 * ipow2(spins);
}
