Code Copyright
--------------
Copyright (c) 2018, Johannes Bausch, jkrb2@cam.ac.uk.

Libraries
---------
Eigen: licensed under MPL2.
Pagmo: Copyright (c) 2018, the Pagmo development team. 
QPP: distributed under the MIT license.
