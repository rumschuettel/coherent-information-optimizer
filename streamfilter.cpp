#include "streamfilter.h"

namespace print_multithreaded {
    std::mutex print_mutex;
    std::map<std::thread::id, size_t> lookup;
    std::thread::id last_id;
    size_t id_incr = 1u;
}
