#include "quantum.h"
#include "utils.h"

#include "qpp/include/qpp.h"
#include <unsupported/Eigen/KroneckerProduct>

#include <iostream>

const cmat ID = qpp::gt.Id<cmat>(2);
const cmat SX = qpp::gt.X;
const cmat SY = qpp::gt.Y;
const cmat SZ = qpp::gt.Z;

const ket Ket0 = qpp::st.z0;
const ket Ket1 = qpp::st.z1;
const ket KetP = qpp::st.x0;
const ket KetM = qpp::st.x1;

const cmat D1 = (cmat(3,2) << 1,0,0,1,0,0).finished();
const cmat D2 = (cmat(3,2) << 1,0,0,-1,0,0).finished();
const cmat D3 = (cmat(3,2) << 0,0,0,0,1,0).finished();
const cmat D4 = (cmat(3,2) << 0,0,0,0,0,1).finished();

const cmat Rho00 = ket2dm(Ket0);
const cmat Rho11 = ket2dm(Ket1);
const cmat Rho01 = ket2dm(Ket0, Ket1);
const cmat Rho10 = ket2dm(Ket1, Ket0);
const cmat RhoP = ket2dm(KetP);
const cmat RhoM = ket2dm(KetM);

const channel ch_dummy = identity_channel(1);

const scalar J = scalar(0., 1.);

using Eigen::kroneckerProduct;


// density matrix for ket
cmat ket2dm(const ket& psi)
{
    return kroneckerProduct(psi, psi.adjoint());
}
cmat ket2dm(const qpp::ket& psi_a, const qpp::ket& psi_b)
{
	return kroneckerProduct(psi_a, psi_b.adjoint());
}


// depolarizing channel as a list of Kraus matrices
channel qubit_depolarizing_channel(const double p)
{
    return {
        std::sqrt(1.-p*3./4.) * ID.sparseView(),
        std::sqrt(p*3./4./3.) * SX.sparseView(),
        std::sqrt(p*3./4./3.) * SY.sparseView(),
        std::sqrt(p*3./4./3.) * SZ.sparseView()
    };
}

// generalized amplitude channel as a list of Kraus matrices
channel generalized_amplitude_dampening_channel(const double gamma, const double N)
{
	return {
		std::sqrt(1-N) * (Rho00 + std::sqrt(1-gamma) * Rho11).sparseView(),
		std::sqrt(gamma*(1-N)) * Rho01.sparseView(),
		std::sqrt(N) * (std::sqrt(1-gamma) * Rho00 + Rho11).sparseView(),
		std::sqrt(gamma * N) * Rho10.sparseView()
	};
}

// dephrasure channel as a list of Kraus operators
channel dephrasure_channel(const double p, const double q)
{
	return {
		std::sqrt((1-p)*(1-q)) * D1.sparseView(),
		std::sqrt(p*(1-q)) * D2.sparseView(),
		std::sqrt(q) * D3.sparseView(),
		std::sqrt(q) * D4.sparseView()
	};
}

// identity channel
channel identity_channel(const size_t d, const size_t n)
{
	return { qpp::gt.Id<cmat>(ipowk(d, n)).sparseView() };
}
channel identity_channel(const ivec& dims, const size_t n)
{
	assert(dims.size() == 2);
	const ivec full_dims{
		ipowk(dims[0], n),
		ipowk(dims[1], n)
	};
	size_t smaller_dim = std::min(full_dims[0], full_dims[1]);
	
	// we assume that the identity channel for a non-square channel is one where
	// the upper left square block is an identity matrix
	cmat out(full_dims[0], full_dims[1]);
	out.block(0, 0, smaller_dim, smaller_dim) = qpp::gt.Id<cmat>(smaller_dim);
	return { out.sparseView() };
}
channel identity_channel(const channel& other, const size_t n)
{
    return identity_channel({
		static_cast<size_t>(other[0].rows()),
		static_cast<size_t>(other[0].cols())
	}, n);
}

// tensor product for Krauss representation of channel
channel tensor(const channel& A, const channel& B)
{
    channel out;
    out.reserve(A.size() * B.size());

    for (auto& a : A)
        for (auto& b : B)
            out.emplace_back( kroneckerProduct<smat, smat>(a, b) );

    return out;
}

// tensor power for Krauss representation of channel
channel tensor(const channel& C, const size_t power)
{
    channel out = C;

    for (auto i = 0; i < power-1; i ++)
        out = tensor(out, C);

    return out;
}

// kraus to superoperator
superoperator kraus2super(const channel& C)
{
    const auto rows = C[0].rows(), columns = C[0].cols();
    superoperator out(rows*rows, columns*columns);

    for (const auto& c : C) {
        const auto ctc = kroneckerProduct(c.conjugate(), c).eval();
        out += ctc;
    }

    out.makeCompressed();
    return out;
}

// qpp::apply with kraus operators is slow, so we use superoperators
cmat apply_super(const superoperator& C, const cmat& rho)
{
    assert(rho.rows() == rho.cols());
    assert(rho.rows()*rho.rows() == C.cols());

    auto d_in = rho.rows();
    auto d_out = isqrt(C.rows());
    cmat out(d_out, d_out);

    Eigen::Map<const ket> vin(rho.transpose().data(), d_in*d_in, 1);
    Eigen::Map<ket> vout(out.transpose().data(), d_out*d_out, 1);
    vout = C*vin;

    return out;
}

// singular or eigenvalues
rvec svals(const cmat& A) {
	Eigen::SelfAdjointEigenSolver<cmat> eigensolver(A, Eigen::EigenvaluesOnly);
	if (eigensolver.info() != Eigen::Success) {
		// try complex variant
		Eigen::ComplexEigenSolver<cmat> eigensolver(A, Eigen::EigenvaluesOnly);
		if (eigensolver.info() != Eigen::Success) {
			// fall back to full SVD
			Eigen::JacobiSVD<cmat, Eigen::NoQRPreconditioner> eigensolver(A);
			return eigensolver.singularValues().cwiseAbs();
		}
		
		return eigensolver.eigenvalues().real();
	}
	
	return eigensolver.eigenvalues();
}

// matrix logarithm, assumes self-adjoint PSD matrix
cmat mlog(const cmat& A) {
    auto decomp = Eigen::SelfAdjointEigenSolver<cmat>(A, Eigen::ComputeEigenvectors);
    auto evals = decomp.eigenvalues().real();
    assert(evals.minCoeff() > 0.);

    auto V = decomp.eigenvectors();
    assert(V.isUnitary());
    assert(A.isApprox(V * evals.asDiagonal() * V.adjoint(), 1e-12));

    auto D = evals.array().unaryExpr<double(*)(double)>(&std::log2).matrix().asDiagonal();

    return V * D * V.adjoint();
}

// von Neumann entropy
double entropy(const cmat& A) {
    auto ev = svals(A); // get the singular values

    double result = 0;
    for (auto i = 0; i < ev.rows(); ++i)
        if (ev(i) > 0.) // not identically zero
            result -= ev(i) * std::log2(ev(i));

    return result;
}

// derivative of von Neumann entropy
// since the matrix log is the most costly operation,
// pass a series of derivative vectors in so that we can reuse it
dvec entropy_D(const cmat& A, const std::vector<cmat>& Ds) {
    dvec out(Ds.size());
    auto L = mlog(A);
    for (auto i = 0; i < Ds.size(); i ++)
        out[i] = -(Ds[i] * L).trace().real();

    return out;
}

// conditional entropy
double conditional_entropy(const cmat& rho_AB, const ivec& B, const ivec& dims)
{
    const auto rho_A = qpp::ptrace(rho_AB, B, dims);
    return entropy(rho_AB) - entropy(rho_A);
}

// coherent information, superoperator version, explicit dimensions
double coherent_information(const superoperator& C, const cmat& rho, const ivec& B, const ivec& dims)
{
    return -conditional_entropy(apply_super(C, rho), B, dims);
}

// coherent information, superoperator version
double coherent_information(const superoperator& C, const cmat& rho, const size_t sys, const size_t env, const size_t sys_d, const size_t env_d)
{
    return coherent_information(C, rho, {1}, {ipowk(sys_d, sys), ipowk(env_d, env)});
}

// conditional entropy derivative
dvec conditional_entropy_D(const cmat& rho_AB, const std::vector<cmat>& sigma_ABs, const ivec& B, const ivec& dims)
{
    const auto d = sigma_ABs.size();

    const auto rho_B = qpp::ptrace(rho_AB, B, dims);
    std::vector<cmat> sigma_Bs(d);
    for (auto i = 0; i < d; i ++)
        sigma_Bs[i].noalias() = qpp::ptrace(sigma_ABs[i], B, dims);

    dvec S_AB = entropy_D(rho_AB, sigma_ABs), S_B = entropy_D(rho_B, sigma_Bs);
    dvec out(d);
    for (auto i = 0; i < d; i ++)
        out[i] = S_AB[i] - S_B[i];
    return out;
}

// coherent information derivative, superoperator version, explicit dimensions
dvec coherent_information_D(const superoperator& C, const cmat& rho, const std::vector<cmat>& sigmas, const ivec& B, const ivec& dims)
{
    const auto d = sigmas.size();

    std::vector<cmat> sigmas_prime(d);
    for (auto i = 0; i < d; i ++)
        sigmas_prime[i].noalias() = apply_super(C, sigmas[i]);

    dvec S = conditional_entropy_D(apply_super(C, rho), sigmas_prime, B, dims);
    std::transform(S.begin(), S.end(), S.begin(), std::negate<double>());
    return S;
}

// coherent information derivative, superoperator version
dvec coherent_information_D(const superoperator& C, const cmat& rho, const std::vector<cmat>& sigmas, const size_t sys, const size_t env, const size_t sys_d, const size_t env_d)
{
    return coherent_information_D(C, rho, sigmas, {1}, {ipowk(sys_d, sys), ipowk(env_d, env)});
}

// coherent information of channel
double coherent_information(const superoperator& C, const size_t rounds, const size_t sys, const size_t env, const size_t sys_d, const size_t env_d)
{
    auto ci = 0.;
    for (auto i = 0; i < rounds; i ++)
        ci += coherent_information(C, ket2dm(qpp::randket(ipow2(sys + env))), sys, env, sys_d, env_d);
    return ci/rounds;
}

// coherent information, channel version
double coherent_information(const channel& C, const cmat& rho, const size_t sys, const size_t sys_d)
{
    // not sure this function is correctly implemented for sys_d != 2
    // depends on whether qpp::apply works with a non-dimension-preserving operation
    assert(sys_d == 2);

    const size_t spins = ilog2(rho.rows());
    assert(sys >= 1 && sys < spins);
    cmat copy = rho;

    // densify channel
    std::vector<cmat> C_dense;
    C_dense.reserve(C.size());
    for (const auto& k : C)
        C_dense.emplace_back(k);

    // apply krauss operators successively
    std::vector<size_t> dims(spins, 2);
    for (size_t i = 0; i < sys; i ++) {
        copy = qpp::apply(copy, C_dense, {i}, dims);
        dims[i] = sys_d;
    }

    return -conditional_entropy(copy, {1}, {ipowk(sys_d, sys), ipow2(spins-sys)}); 
}

// repetition encode a state
ket repetition_encode(const ket& psi, const size_t n)
{
    auto d = psi.rows();
    ket out = qpp::mket( ivec(n, 0) ) * psi(0);
    for (auto i = 1; i < d; i ++)
        out.noalias() += qpp::mket( ivec(n, i) ) * psi(i);
    return out;
}

// display state
#include <limits>
std::string pretty_print(const ket& psi, const size_t spins, const double threshold, const size_t precision, const size_t sys, const size_t env, const size_t sys_d, const size_t env_d)
{
    using std::abs;

    std::ostringstream out;

    // less than 100 colums width
    size_t width = 80u - 2*precision - spins;

    double max = psi.redux([](const auto& a, const auto& b){ return abs(a) > abs(b) ? abs(a) : abs(b); }).real();
    if (max == 0.)
        return "";
    
    out.precision(precision);
    out << std::scientific;
    for (size_t i = 0; i < psi.rows(); i ++) {
        const scalar& v = psi(i);

        double bar_length = abs(v) * static_cast<double>(width);
        if (bar_length < threshold) continue;
        bar_length /= max;

        // print basis state in form 0121 0001, split across sys and env
        size_t dleft = i;
        for (size_t j = 0; j < sys; j ++) {
            out << dleft % sys_d;
            dleft /= sys_d;
        }
        out << "-";
        for (size_t j = 0; j < env; j ++) {
            out << dleft % env_d;
            dleft /= env_d;
        }
        assert(dleft == 0);

        // print weight
        out << ": ";
        out << std::setfill(' ') << std::showpos;
        out << std::setw(precision+10) << v.real() << std::setw(precision+10) << v.imag() << "i |";

        // print bar
        for (; bar_length > 1.; bar_length -= 1.) out << "█";
        if (bar_length > .75) out << "▓";
        else if (bar_length > .5) out << "▒";
        else if (bar_length > .25) out << "░";
        out << "\n";
    }

    return out.str();
}

superoperator channel_and_env(const channel& C_single, const size_t copies, const size_t env, const bool optimize)
{
    assert(copies >= 1 && env >= 1);
	
	const channel id_env = identity_channel(2, env);
	const size_t larger_dim = std::max(C_single[0].rows(), C_single[0].cols());

    // we sparsify the matrix after each multiplication to save memory and speed up further usage
    const auto keeper = [] (auto _, auto __, const scalar& val) { return abs(val) > 0.; };

    superoperator C = kraus2super(tensor(identity_channel(2, copies), id_env));
    for (size_t i = 0; i < copies; i ++) {
		const channel C1 = identity_channel(larger_dim, i);
		const channel C2 = identity_channel(2, copies - 1 - i);
        C = kraus2super(
			tensor(tensor(tensor(C1, C_single), C2), id_env)
		) * C;
        if (optimize) C.prune(keeper); // save memory on the fly            
    }

    C.prune(keeper);
    return C;
}

