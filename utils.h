#pragma once

#include <stddef.h>
#include <cmath>
#include <vector>
//
// power of 2
inline size_t ipow2(const size_t n)
{
    return 1<<n;
}

// power of k
inline size_t ipowk(const size_t k, const size_t n)
{
    return n > 0 ? k * ipowk(k, n-1) : 1;
}

// log of 2
inline size_t ilog2(const size_t d)
{
    return std::ilogb(d);
}

// integer sqrt
inline size_t isqrt(const size_t d)
{
    return static_cast<size_t>(std::round(std::sqrt(d)));
}

inline double ReLU(const double x)
{
    return (x > 0.) ? x : 0.;
}

inline double Identity(const double x)
{
    return x;
}

inline double Tanh(const double x)
{
    return std::tanh(x);
}

inline double Cos(const double x)
{
    return std::cos(x);
}
