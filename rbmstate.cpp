#include "rbmstate.h"

#include "quantum.h"

namespace {
    rvec dec2vec(unsigned int dec, size_t n)
    {
        rvec out(n);
        for (auto i = 0; i < n; i ++)
            out(n-i-1) = static_cast<double>( (dec>>i) & 0x01 );
        return out;
    }
}

RBMState::RBMState() = default;

ket RBMState::From(const ket& a, const ket& b, const cmat& W, const bool as_schmidt)
{
    assert(W.size() == a.size()*b.size());
    const size_t spins = a.size();

    ket out(ipow2(spins * (as_schmidt ? 2 : 1)));

    if (as_schmidt)
        out.setZero();

    for (auto i = 0; i < ipow2(spins); i ++)
        out(as_schmidt ? (ipow2(spins)*i + i) : i) = Weight(dec2vec(i, spins), a, b, W);

    return out.normalized();
}

ket RBMState::From(const std::vector<double>& parameters, const bool as_schmidt, const size_t hidden_layer_width)
{
    // get complex values from real parameters
    // for Schmidt ansatz all those parameters are real
    auto n_params = parameters.size() / (as_schmidt ? 1 : 2);
    std::vector<scalar> complex_parameters(n_params);
    if (as_schmidt)
        for (auto i = 0; i < n_params; i ++)
            complex_parameters[i] = parameters[i] + J*0.;
    else
        for (auto i = 0; i < n_params; i ++)
            complex_parameters[i] = parameters[2*i] + J*parameters[2*i + 1];

    size_t D, Dh;
    if (hidden_layer_width == 0) { // hidden layer same width
        // size of vector a; since D = d_a = d_b and d_W = d_a*d_a, solve for D
        D = Dh = isqrt(1 + n_params) - 1;
    } else {
        Dh = hidden_layer_width;
        D = (n_params - Dh) / (1 + Dh);
    }
    assert(D >= 1 && Dh >= 1 && (n_params == D + Dh + D*Dh));

    // map data to some vectors without copying
    Eigen::Map<const ket> view(complex_parameters.data(), n_params, 1);
    auto a = view.block(0, 0, D, 1);
    auto b = view.block(D, 0, Dh, 1);
    auto W = Eigen::Map<const cmat>(complex_parameters.data()+D+Dh, Dh, D);


    return From(a, b, W, as_schmidt);
}

size_t RBMState::ParameterCount(const size_t spins, const bool as_schmidt, const size_t hidden_layer_width)
{
    const size_t Dh = hidden_layer_width > 0 ? hidden_layer_width : spins;
    // 2 complex vectors, 1 complex matrix 
    return (spins + Dh + spins*Dh) * (as_schmidt ? 1 : 2);
}

scalar RBMState::Weight(const ket& in, const ket& a, const ket& b, const cmat& W)
{
    scalar F = 1.;
    ket prod = W * in;
    assert(prod.size() == b.size());
    for (auto i = 0; i < b.size(); i ++)
        F *= 2.*cosh(b(i) + prod(i));
    return exp(static_cast<scalar>(in.dot(a))) * F;
}
