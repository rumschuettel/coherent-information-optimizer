#pragma once

#include "quantum.h"

// this is code from gradients_and_hessians.hpp
// pagmo is terrible with compilation performance, so we can just have this
// function separately here
using vector_double = dvec;
template <typename Func>
vector_double estimate_gradient_h(Func f, const vector_double &x, double dx = 1e-2)
{
    vector_double f0 = f(x);
    vector_double gradient(f0.size() * x.size(), 0.);
    vector_double x_r1 = x, x_l1 = x;
    vector_double x_r2 = x, x_l2 = x;
    vector_double x_r3 = x, x_l3 = x;
    // We change one by one each variable by dx and estimate the derivative
    for (decltype(x.size()) j = 0u; j < x.size(); ++j) {
        double h = std::max(std::abs(x[j]), 1.0) * dx;
        x_r1[j] = x[j] + h;
        x_l1[j] = x[j] - h;
        x_r2[j] = x[j] + 2. * h;
        x_l2[j] = x[j] - 2. * h;
        x_r3[j] = x[j] + 3. * h;
        x_l3[j] = x[j] - 3. * h;
        vector_double f_r1 = f(x_r1);
        vector_double f_l1 = f(x_l1);
        vector_double f_r2 = f(x_r2);
        vector_double f_l2 = f(x_l2);
        vector_double f_r3 = f(x_r3);
        vector_double f_l3 = f(x_l3);
        if (f_r1.size() != f0.size() || f_l1.size() != f0.size() || f_r2.size() != f0.size() || f_l2.size() != f0.size()
                || f_r3.size() != f0.size() || f_l3.size() != f0.size()) {
            throw std::invalid_argument("Change in the size of the returned vector detected around the "
                    "reference point. Cannot compute a gradient");
        }
        for (decltype(f_r1.size()) i = 0u; i < f_r1.size(); ++i) {
            double m1 = (f_r1[i] - f_l1[i]) / 2.;
            double m2 = (f_r2[i] - f_l2[i]) / 4.;
            double m3 = (f_r3[i] - f_l3[i]) / 6.;
            double fifteen_m1 = 15. * m1;
            double six_m2 = 6. * m2;
            double ten_h = 10. * h;
            gradient[j + i * x.size()] = ((fifteen_m1 - six_m2) + m3) / ten_h;
        }
        x_r1[j] = x[j];
        x_l1[j] = x[j];
        x_r2[j] = x[j];
        x_l2[j] = x[j];
        x_r3[j] = x[j];
        x_l3[j] = x[j];
    }
    return gradient;
}

template <typename Func>
vector_double estimate_gradient(Func f, const vector_double &x, double dx = 1e-8)
{
    vector_double f0 = f(x);
    vector_double gradient(f0.size() * x.size(), 0.);
    vector_double x_r = x, x_l = x;
    // We change one by one each variable by dx and estimate the derivative
    for (decltype(x.size()) j = 0u; j < x.size(); ++j) {
        double h = std::max(std::abs(x[j]), 1.0) * dx;
        x_r[j] = x[j] + h;
        x_l[j] = x[j] - h;
        vector_double f_r = f(x_r);
        vector_double f_l = f(x_l);
        if (f_r.size() != f0.size() || f_l.size() != f0.size()) {
            throw std::invalid_argument("Change in the size of the returned vector detected around the "
                                               "reference point. Cannot compute a gradient");
        }
        for (decltype(f_r.size()) i = 0u; i < f_r.size(); ++i) {
            gradient[j + i * x.size()] = (f_r[i] - f_l[i]) / 2. / h;
        }
        x_r[j] = x[j];
        x_l[j] = x[j];
    }
    return gradient;
}

