cmake_minimum_required(VERSION 3.1)

# living on the edge
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -mtune=native")

project(coherent)
add_executable(coherent main.cpp quantum.cpp rbmstate.cpp rbmstateproblem.cpp nn.cpp ffstate.cpp ffstateproblem.cpp rawstate.cpp rawstateproblem.cpp tests.cpp streamfilter.cpp)
target_include_directories(coherent PRIVATE ./packages/include) # pagmo2
target_include_directories(coherent PRIVATE ./packages/eigen) # eigen3 is header-only

# eigen
find_package(Eigen3 3.3 NO_MODULE)
if (TARGET Eigen3::Eigen)
	target_link_libraries(coherent Eigen3::Eigen)
endif (TARGET Eigen3::Eigen)

# nlopt
find_library(NLOPT_LIB nlopt)
target_link_libraries(coherent ${NLOPT_LIB})
# threading building blocks
find_library(TBB_LIB tbb)
target_link_libraries(coherent ${TBB_LIB})
# pagmo
find_library(PAGMO_LIB pagmo ./packages/lib)
target_link_libraries(coherent ${PAGMO_LIB})

# boost
find_package(Boost 1.67 COMPONENTS program_options serialization REQUIRED)
target_include_directories(coherent PRIVATE ${Boost_INCLUDE_DIR})
target_link_libraries(coherent ${Boost_LIBRARIES})

# need threads
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(coherent Threads::Threads)

install(TARGETS coherent RUNTIME DESTINATION bin)
