#include "rbmstateproblem.h"
#include "rbmstate.h"

#include "utils-gradients.h"

#include <iostream>
#include "qpp/include/qpp.h"
RBMStateCoherentInformationProblem::RBMStateCoherentInformationProblem(const size_t spins, const channel& C_single, const bool as_schmidt)
    : as_schmidt{ as_schmidt },
    n_spins{ spins },
    total_spins{ spins * (as_schmidt ? 2 : 1) },
    n_sys{ total_spins>>1 },
    n_env{ total_spins>>1 },
    d_sys( C_single[0].rows() ), // allow narrowing
    n_params{ RBMState::ParameterCount(n_spins, as_schmidt, (as_schmidt ? 3 : 1.5)*n_spins) },
    C{ channel_and_env(C_single, n_sys, n_env) }
{
    assert(total_spins % 2 == 0);
}

std::string RBMStateCoherentInformationProblem::get_name() const
{
    return std::string("coherent information RBMState problem") + (as_schmidt ? " (Schmidt)" : "");
}

dvec RBMStateCoherentInformationProblem::fitness(const dvec& params) const
{
    auto state = RBMState::From(params, as_schmidt, (as_schmidt ? 3 : 1.5)*n_spins);
    // pagmo minimizes by default
    dvec objective = {
        -coherent_information(C, ket2dm(state), n_sys, n_env, d_sys, 2)/n_sys
    };
    return objective;
}

dvec RBMStateCoherentInformationProblem::gradient(const dvec& params) const
{
    // better would be to have an analytical gradient
    return estimate_gradient_h([this](const dvec& params) {
            return fitness(params);
            }, params, 1e-4);
}


std::pair<dvec, dvec> RBMStateCoherentInformationProblem::get_bounds() const
{
    return { dvec(n_params, -10.), dvec(n_params, 10.) };
}


