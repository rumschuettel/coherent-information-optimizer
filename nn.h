#pragma once

#include "quantum.h"

using ActivationFunction = double (*)(const double);

struct Layer {
    const size_t in, out;
    const ActivationFunction f;

    Layer(const size_t in, const size_t out, const ActivationFunction f);

    size_t ParameterCount() const;
    rvec Transform(const rvec& a, const std::vector<double>& parameters, const size_t offset) const;
};
