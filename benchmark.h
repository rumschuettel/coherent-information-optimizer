// code idea from https://stackoverflow.com/questions/2808398/easily-measure-elapsed-time

#include <chrono>
#include <utility>
template<typename TimeT = std::chrono::milliseconds>
struct measure
{
    template<typename F, typename ...Args>
    static typename TimeT::rep execution(F benchmarked_function, Args&&... arguments)
    {
        auto before = std::chrono::system_clock::now();
        benchmarked_function(std::forward<Args>(arguments)...);
        auto duration = std::chrono::duration_cast<TimeT>(std::chrono::system_clock::now() - before);
        return duration.count();
    }
};
