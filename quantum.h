#pragma once

#include "utils.h"

#include <Eigen/Dense>
#include <Eigen/Sparse>

// bug in qpp for cerr, need iostream before
#include <iostream>
#include "qpp/include/types.h"


using qpp::cmat;
using qpp::ket;
using qpp::bra;

using scalar = std::complex<double>;
using smat = Eigen::SparseMatrix<scalar>;
using channel = std::vector<smat>;
using superoperator = smat;

using rvec = qpp::dyn_col_vect<double>;
using ivec = std::vector<qpp::idx>;
using rmat = qpp::dyn_mat<double>;
using dvec = std::vector<double>;

// constants
extern const cmat ID, SX, SY, SZ;
extern const cmat D1, D2, D3, D4;
extern const ket Ket0, Ket1, KetP, KetM;
extern const cmat Rho00, Rho01, Rho10, Rho11, RhoP, RhoM;
extern const scalar J;
extern const channel ch_dummy;

// forward-declarations
cmat ket2dm(const qpp::ket& psi);
cmat ket2dm(const qpp::ket& psi_a, const qpp::ket& psi_b);
channel qubit_depolarizing_channel(const double p);
channel generalized_amplitude_dampening_channel(const double gamma, const double N);
channel dephrasure_channel(const double p, const double q);
channel identity_channel(const size_t d, const size_t n = 1);
channel identity_channel(const ivec& dims, const size_t n = 1);
channel identity_channel(const channel& other, const size_t n = 1);
channel tensor(const channel& A, const channel& B);
channel tensor(const channel& C, const size_t power);
superoperator kraus2super(const channel& C);
cmat apply_super(const superoperator& C, const cmat& rho);
rvec svals(const cmat& A);

double entropy(const cmat& A);
dvec entropy_D(const cmat& A, const std::vector<cmat>& Ds);
double conditional_entropy(const cmat& rho_AB, const ivec& B, const ivec& dims);
dvec conditional_entropy_D(const cmat& rho_AB, const std::vector<cmat>& sigma_ABs, const ivec& B, const ivec& dims);
double coherent_information(const superoperator& C, const cmat& rho, const ivec& B, const ivec& dims);
double coherent_information(const superoperator& C, const cmat& rho, const size_t sys, const size_t env, const size_t sys_d=2, const size_t env_d=2);
dvec coherent_information_D(const superoperator& C, const cmat& rho, const std::vector<cmat>& sigmas, const ivec& B, const ivec& dims);
dvec coherent_information_D(const superoperator& C, const cmat& rho, const std::vector<cmat>& sigmas, const size_t sys, const size_t env, const size_t sys_d=2, const size_t env_d=2);

double coherent_information(const superoperator& C, const size_t rounds, const size_t sys, const size_t env, const size_t sys_d=2, const size_t env_d=2);
double coherent_information(const channel& C, const cmat& rho, const size_t sys, const size_t sys_d=2);
ket repetition_encode(const ket& psi, const size_t n);
std::string pretty_print(const ket& psi, const size_t spins, const double threshold = .01, const size_t precision = 6, const size_t sys=2, const size_t env=2, const size_t sys_d=2, const size_t env_d=2);
superoperator channel_and_env(const channel& C_single, const size_t copies, const size_t env, const bool optimize = true);
