#pragma once

#include "quantum.h"

class RBMStateCoherentInformationProblem
{
protected:
    const bool as_schmidt;
    const size_t total_spins;
    const size_t n_spins;
    const size_t n_sys;
    const size_t n_env;
    const size_t d_sys;
    const size_t n_params;

    const superoperator C;
public:
    // note that we need a default constructor for rvalue forwarding
    RBMStateCoherentInformationProblem(const size_t spins = 1, const channel& C_single = ch_dummy, const bool as_schmidt = false);
	
    std::string get_name() const;
    dvec fitness(const dvec& params) const;
    dvec gradient(const dvec& params) const;
    std::pair<dvec, dvec> get_bounds() const;
};


