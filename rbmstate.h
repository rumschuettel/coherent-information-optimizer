#pragma once

#include "quantum.h"

class RBMState
{
    private:
        RBMState();

    public:
        static ket From(const ket& a, const ket& b, const cmat& W, const bool as_schmidt);
        static ket From(const std::vector<double>& parameters, const bool as_schmidt, const size_t hidden_layer_width);

        static size_t ParameterCount(const size_t spins, const bool as_schmidt, const size_t hidden_layer_width);

    protected:
        static scalar Weight(const ket& in, const ket& a, const ket& b, const cmat& W);
        // converts the number, e.g. 5, to a state {0, 1, 0, 1} of length n
        static ket dec2ket(unsigned int dec, size_t n);
};


