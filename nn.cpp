#include "nn.h"

Layer::Layer(const size_t in, const size_t out, ActivationFunction f)
    : in(in), out(out), f(f)
{}

size_t Layer::ParameterCount() const
{
    // in*out for matrix, out for bias
    return in*out + out;
}

rvec Layer::Transform(const rvec& a, const std::vector<double>& parameters, const size_t offset) const
{
    assert(parameters.size()-offset >= ParameterCount());

    // map data to some vectors without copying
    Eigen::Map<const rmat> W(parameters.data() + offset, out, in);
    Eigen::Map<const rmat> b(parameters.data() + offset + in*out, out, 1);

    return (W*a + b).unaryExpr(f);
}

