# COHERENT
## Dependencies

Clone eigen3 to eigen/, e.g. by using

    hg clone https://bitbucket.org/eigen/eigen/ packages/eigen
    cd packages/eigen
    hg update 3.3

Clone quantum++ to qpp

    git clone --depth 1 https://github.com/vsoftco/qpp.git

Install boost > 1.67 system-wide.
Install Intel threading building blocks, e.g. by

	conda install -c intel tbb-devel tbb
	
Install nlopt, e.g. by

	conda install -c conda-forge nlopt
	
Extract pagmo 2 to `packages/pagmo2/` or install headers globally; if extracting manually, use

	ccmake  # configure use of e.g. nlopt and eigen
	cmake -DCMAKE_INSTALL_PREFIX=../
	make install

See https://esa.github.io/pagmo2/install.html.
For pagmo to not interleave prints from multiple threads, apply `pagmo.patch.1` to `./packages/include/pagmo/io.hpp`.

To make a release build,

    mkdir -p build/debug
    cd build/debug
    cmake ../.. -DCMAKE_BUILD_TYPE=Release
    make

To speed up building and linking, use `-j4` on make and `gold` instead of `ld` as a linker.
