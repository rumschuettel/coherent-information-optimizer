#include "ffstateproblem.h"
#include "ffstate.h"

#include "utils-gradients.h"

#include <iostream>
#include "qpp/include/qpp.h"
FFStateCoherentInformationProblem::FFStateCoherentInformationProblem(const FFState& state, const channel& C_single, const bool as_schmidt)
    : as_schmidt{ as_schmidt },
    state{ state },
    n_spins{ state.Spins() * (as_schmidt ? 2 : 1) },
    n_sys{ n_spins>>1 },
    n_env{ n_spins>>1 },
    d_sys( C_single[0].rows() ), // allow narrowing
    n_params{ state.ParameterCount() },
    C{ channel_and_env(C_single, n_sys, n_env) }
{
    assert(n_spins % 2 == 0);
}

std::string FFStateCoherentInformationProblem::get_name() const
{
    return std::string("coherent information FFState problem") + (as_schmidt ? " (Schmidt)" : "");
}

dvec FFStateCoherentInformationProblem::fitness(const dvec& params) const
{
    dvec objective = {
        -coherent_information(C, ket2dm(state.From(params, as_schmidt)), n_sys, n_env, d_sys, 2)/n_sys
    };
    return objective;
}

dvec FFStateCoherentInformationProblem::gradient(const dvec& params) const
{
    // better would be to have an analytical gradient
    return estimate_gradient_h([this](const dvec& params) {
            return fitness(params);
            }, params, 1e-5);
}


std::pair<dvec, dvec> FFStateCoherentInformationProblem::get_bounds() const
{
    return { dvec(n_params, -5.), dvec(n_params, 5.) };
}


