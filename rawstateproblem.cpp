#include "rawstateproblem.h"
#include "rawstate.h"

#include "utils-gradients.h"

#include <iostream>
#include "qpp/include/qpp.h"
RawStateCoherentInformationProblem::RawStateCoherentInformationProblem(const size_t spins, const channel& C_single)
	: n_spins{ spins },
    n_sys{ spins>>1 },
    n_env{ spins>>1 },
    d_sys( C_single[0].rows() ), // allow narrowing
    n_params{ RawState::ParameterCount(spins) }, 
    C{ channel_and_env(C_single, n_sys, n_env) }
{
    assert(spins % 2 == 0);
}

std::string RawStateCoherentInformationProblem::get_name() const
{
    return "coherent information raw state problem";
}

dvec RawStateCoherentInformationProblem::fitness(const dvec& params) const
{
    auto state = RawState::From(params);
    // pagmo minimizes by default
    dvec objective = {
        -coherent_information(C, ket2dm(state), n_sys, n_env, d_sys, 2)/n_sys
    };
    return objective;
}

#include <iostream>
dvec RawStateCoherentInformationProblem::gradient(const dvec& params) const
{
    // analytical gradient
    auto state = RawState::From(params);
    std::vector<cmat> sigmas(n_params);
    for (auto i = 0; i < n_params; i ++) {
        auto der = RawState::DFrom(i, n_spins);
        // derivative of |x><x| is |x'><x| + |x><x'|
        sigmas[i].noalias() = der * state.adjoint() + state * der.adjoint();
    }

    dvec objective = coherent_information_D(C, ket2dm(state), sigmas, n_sys, n_env);
    // flip gradient since pagmo minimizes
    for (auto i = 0; i < n_params; i ++) {
        objective[i] /= n_sys;
        objective[i] *= -1.;
    }
    return objective;
}


std::pair<dvec, dvec> RawStateCoherentInformationProblem::get_bounds() const
{
    return { dvec(n_params, -1.), dvec(n_params, 1.) };
}


