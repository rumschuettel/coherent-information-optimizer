#pragma once

#include "quantum.h"

class RawState
{
    private:
        RawState();

    public:
        static ket From(const std::vector<double>& parameters);
        static ket DFrom(const size_t i, const size_t spins);
        static size_t ParameterCount(const size_t spins);
};


