#include "ffstate.h"

namespace {
    rvec dec2vec(unsigned int dec, size_t n)
    {
        rvec out(n);
        for (auto i = 0; i < n; i ++)
            out(n-i-1) = static_cast<double>( (dec>>i) & 0x01 );
        return out;
    }
}

FFState::FFState(const std::vector<Layer> &layers)
    : layers(layers), spins(layers[0].in)
{
    for (auto i = 0; i < layers.size()-1; i ++)
        assert(layers[i].out == layers[i+1].in);
    assert(layers.back().out == 1 || layers.back().out == 2);
}

scalar FFState::Weight(const rvec& in, const std::vector<double>& parameters) const
{
    size_t offset = 0;
    
    rvec current = in;
    for (const auto &l : layers) {
        current = l.Transform(current, parameters, offset);
        offset += l.ParameterCount();
    }

    // if last layer has size 1, only produce real params
    if (current.size() == 2)
        return exp(current(0) + J*current(1));
    return exp(current(0));
}

ket FFState::From(const std::vector<double> &parameters, const bool as_schmidt) const
{
    assert(parameters.size() == ParameterCount());
    ket out(ipow2(spins * (as_schmidt ? 2 : 1)));

    if (as_schmidt)
        out.setZero();

    for (auto i = 0; i < ipow2(spins); i ++)
        out(as_schmidt ? (ipow2(spins)*i + i) : i) = Weight(dec2vec(i, spins), parameters);

    return out.normalized();
}

size_t FFState::ParameterCount() const
{
    size_t count = 0;
    for (const auto &l : layers)
        count += l.ParameterCount();
    return count;
}

size_t FFState::Spins() const
{
    return layers[0].in;
}
