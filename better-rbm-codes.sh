#!/bin/bash

# default vars
PMIN=0.25
PSTEP=0.00005
PMAX=0.255

SPINS=4
THREADS=100
GENERATIONS=1000
POPULATION=250
VERBOSITY=10
LOCALEVALS=25
LOCALERROR=0.00001

COMPUTER=`cat /etc/hostname`
EXECUTABLE="./coherent"
TEST=0

# load from command line
OPTIND=1

while getopts "s:e:x:n:t" opt; do
    case "$opt" in
    s)  PMIN=$OPTARG
        ;;
    e)  PMAX=$OPTARG
        ;;
    x)  EXECUTABLE=$OPTARG
        ;;
    n)  THREADS=$OPTARG
        ;;
    t)  TEST=1
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

# program
for p in $(seq $PMIN $PSTEP $PMAX)
do
    IDSTRING="$COMPUTER.n$SPINS.p$p.g$GENERATIONS.pop$POPULATION"
    COMMAND="nice -19 $EXECUTABLE -s$SPINS -p$p --threads=$THREADS --population=$POPULATION --generations=$GENERATIONS -v$VERBOSITY --error=$LOCALERROR --maxeval=$LOCALEVALS --threshold=0 --precision=5 --file=$IDSTRING.run > $IDSTRING.out 2> $IDSTRING.err"
    echo $COMMAND
    if [ $TEST -eq 0 ]; then
        eval $COMMAND
    fi
done

