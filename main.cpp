#include "benchmark.h"
#include "quantum.h"
#include "utils.h"
#include "tests.h"

#include "rawstate.h"
#include "rawstateproblem.h"
#include "rbmstate.h"
#include "rbmstateproblem.h"
#include "ffstateproblem.h"

#include <complex>
#include <iostream>
#include <fstream>
#include <functional>

using std::cout;
using std::endl;
using std::flush;

//////////////////
// app code
#include <boost/program_options.hpp>

template<typename ostream_t>
int preset(const std::string& which, ostream_t&& out, const boost::program_options::variables_map& vm);

std::pair<bool, int> setup_command_line_args(int argc, char** argv, boost::program_options::variables_map& vm)
{
    try {
        using boost::program_options::value;
        boost::program_options::options_description desc{"Options"};
        desc.add_options()
            ("help,h", "This help")
            ("test", "Run a sequence of tests")
            ("model", value<std::string>()->default_value("FF"), "Which model to use. Has to be RBM or RBM-Schmidt, FF or FF-Schmidt, or RAW.")
            ("lalg", value<std::string>()->default_value("ccsaq"), "Local algorithm. Not checked for validity.")
            ("spins,s", value<size_t>()->default_value(6), "Number of spins to run the simulation on. Has to be even.")
            ("threads,t", value<size_t>()->default_value(2), "Number of threads")
            ("generations,g", value<size_t>()->default_value(50), "Number of generations per individual")
            ("population,n", value<size_t>()->default_value(10), "Size of population")
            ("error,e", value<double>()->default_value(1e-4), "Local optimization error threshold")
            ("maxeval,m", value<size_t>()->default_value(200), "Maximum number of local optimization steps")
            ("verbosity,v", value<size_t>()->default_value(1), "Output verbosity")
            ("precision", value<size_t>()->default_value(4), "Print precision for final state")
            ("threshold", value<double>()->default_value(0.0), "Print threshold for final state")
			("channel", value<std::string>()->default_value("dep"), "Channel. dep, gadc, or dephrasure")
            ("parameter1", value<double>()->default_value(0.25), "Channel parameter 1. p for dep, gamma for gadc, p for dephrasure")
			("parameter2", value<double>()->default_value(0.), "Channel parameter 2. Unused for dep, N for gadc, q for dephrasure")
            ("file", value<std::string>(), "Write simulation output to file");

        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help") > 0) {
            cout << desc << endl;
            return {true, 0};
        }

        if (vm.count("test") > 0) {
            tests();
            return {true, 0};
        }

        if (vm.count("file") > 0) {
            std::ofstream of;
            of.open(vm["file"].as<std::string>().c_str(), std::ios::out);
            if (!of) {
                cout << "output file cannot be opened for writing." << endl;
                return {true, 1};
            }
        }


        // sanitize
        auto model = vm["model"].as<std::string>();
        if (model != "RBM" && model != "RBM-Schmidt" && model != "FF" && model != "FF-Schmidt" && model != "RAW") {
            cout << "model needs to be RBM or RBM-Schmidt, FF or FF-Schmidt, or RAW" << endl;
            return {true, 1};
        }
        auto channel = vm["channel"].as<std::string>();
		if (channel != "dep" && channel != "gadc" && channel != "dephrasure") {
			cout << "channel has to be either dep, gadc, or dephrasure" << endl;
			return {true, 1};
		}
        if (vm["spins"].as<size_t>() % 2 == 1) {
            cout << "spins needs to be even" << endl;
            return {true, 1};
        }
    } catch (const boost::program_options::error &e) {
        std::cerr << e.what() << endl;
        return {true, 1};
    }

    return {false, 0};
}


#include <pagmo/algorithms/bee_colony.hpp>
#include <pagmo/algorithms/nlopt.hpp>
#include <pagmo/io.hpp>
#include <pagmo/archipelago.hpp>

using ParamsToKetF = std::function<ket(const dvec&)>;
void print_population_statistics(std::ostream& out, ParamsToKetF params_to_ket, const pagmo::population& pop, const size_t spins, const double precision, const size_t threshold)
{
    auto champion_params = pop.champion_x();
    auto best_value = -pop.champion_f()[0];
    const pagmo::problem& problem = pop.get_problem();
    auto n_fevals = problem.get_fevals(),
         n_gevals = problem.get_gevals();

    auto state = params_to_ket(champion_params);
    out << "best CI = " << best_value << " found using " << n_fevals << " f-evals/" << n_gevals << " g-evals\n";
    out << "|psi> = \n";
    out << pretty_print(state, spins, threshold, precision, spins/2, spins/2);
    out << endl;
}

template<typename global_algorithm = pagmo::bee_colony>
void run(pagmo::problem& problem,
         ParamsToKetF params_to_ket,
         const size_t SPINS,
         const size_t GENERATIONS,
         const size_t THREADS,
         const size_t POPULATION,
         const size_t VERBOSITY,
         const double ERROR_THRESHOLD,
         const double EVAL_THRESHOLD,
         const size_t OUT_PRECISION,
         const double OUT_THRESHOLD,
         const std::string& local_algorithm_name = "ccsaq",
         std::ostream& out = std::cout)
{
    using std::cout;

    cout << problem << "\n\n";

    ///////////////////////////////////////
    // global optimization
    cout << "\e[1;31m" << "GLOBAL OPTIMIZATION"  << "\e[0m" << endl;

    pagmo::algorithm global_solver{global_algorithm(GENERATIONS)}; // number of generation
    global_solver.set_verbosity(VERBOSITY);
    pagmo::archipelago archipelago{THREADS, global_solver, problem, POPULATION}; // threads and population size

    pagmo::print(archipelago, "\n");
    cout << measure<std::chrono::milliseconds>::execution( [&]() {
            archipelago.evolve(1);
            archipelago.wait();

            cout << "optimization took ";
        }) << " ms" << endl;
    pagmo::print(archipelago, "\n");

    for (const auto &isl : archipelago) {
        // local optimization of best candidate
        cout << "\n\e[1;31m" << "CANDIDATE FROM GLOBAL OPTIMIZATION"  << "\e[0m\n";

        auto pop = isl.get_population();
        print_population_statistics(out, params_to_ket, pop, SPINS, OUT_PRECISION, OUT_THRESHOLD);
    }
    cout << endl << endl << std::flush;

    ///////////////////////////////////////
    // local optimization
    std::cout << "\n\e[1;31m" << "LOCAL OPTIMIZATION"  << "\e[0m\n";

    auto local_algorithm = pagmo::nlopt(local_algorithm_name);
    local_algorithm.set_xtol_abs(0.); // disable stopping criterion
    local_algorithm.set_xtol_rel(0.); // disable stopping criterion
    local_algorithm.set_selection("best");
    local_algorithm.set_replacement("worst");
    local_algorithm.set_ftol_abs(ERROR_THRESHOLD);
    local_algorithm.set_maxeval(EVAL_THRESHOLD);
    pagmo::algorithm local_solver{local_algorithm};
    local_solver.set_verbosity(VERBOSITY);

    for (auto &isl : archipelago)
        isl.set_algorithm(local_solver);
    pagmo::print(archipelago, "\n");

    cout << measure<std::chrono::milliseconds>::execution( [&]() {
        archipelago.evolve(1);
        archipelago.wait();

        cout << "optimization took ";
    }) << " ms" << endl;
    pagmo::print(archipelago, "\n");

    for (const auto &isl : archipelago) {
        // local optimization of best candidate
        std::cout << "\n\e[1;31m" << "CANDIDATE FROM LOCAL OPTIMIZATION"  << "\e[0m\n";

        auto pop = isl.get_population();
        print_population_statistics(out, params_to_ket, pop, SPINS, OUT_PRECISION, OUT_THRESHOLD);
    }
    cout << "\n\n" << std::flush;
}

int main(int argc, char** argv)
{
    boost::program_options::variables_map options;
    auto [quit, code] = setup_command_line_args(argc, argv, options);
    if (quit)
        return code;

    const std::string MODEL = options["model"].as<std::string>();
	const std::string CHANNEL = options["channel"].as<std::string>();
    const size_t SPINS = options["spins"].as<size_t>();
    const double P1 = options["parameter1"].as<double>();
    const double P2 = options["parameter2"].as<double>();
    const size_t GENERATIONS = options["generations"].as<size_t>();
    const size_t THREADS = options["threads"].as<size_t>();
    const size_t POPULATION = options["population"].as<size_t>();
    const size_t VERBOSITY = options["verbosity"].as<size_t>();
    const double ERROR_THRESHOLD = options["error"].as<double>();
    const double EVAL_THRESHOLD = options["maxeval"].as<size_t>();
    const size_t OUT_PRECISION = options["precision"].as<size_t>();
    const double OUT_THRESHOLD = options["threshold"].as<double>();
    const std::string LALG = options["lalg"].as<std::string>();

    const bool SCHMIDT = MODEL == "FF-Schmidt" || MODEL == "RBM-Schmidt";
    const size_t nn_spins = SCHMIDT ? (SPINS>>1) : SPINS;
	
	// channel
	auto C_single = 
		CHANNEL == "dep" ? 
		qubit_depolarizing_channel(P1) : 
		CHANNEL == "gadc" ?
		generalized_amplitude_dampening_channel(P1, P2) :
		dephrasure_channel(P1, P2);

    // construct problem
    FFState state({
        Layer(nn_spins, nn_spins, Cos),
        Layer(nn_spins, nn_spins, ReLU),
        Layer(nn_spins, nn_spins, ReLU),
		Layer(nn_spins, nn_spins, ReLU),
        Layer(nn_spins, SCHMIDT ? 1 : 2, Identity)
    });
    pagmo::problem problem =
        (MODEL == "RBM" || MODEL == "RBM-Schmidt") ?
        pagmo::problem{RBMStateCoherentInformationProblem(nn_spins, C_single, SCHMIDT)} :
        MODEL == "RAW" ?
        pagmo::problem{RawStateCoherentInformationProblem(SPINS, C_single)} :
        pagmo::problem{FFStateCoherentInformationProblem(state, C_single, SCHMIDT)};

    // construct pretty print callback
    ParamsToKetF params_to_ket;
    if (MODEL == "RAW")
        params_to_ket = [](const dvec& params) { return RawState::From(params); };
    if (MODEL == "RBM" || MODEL == "RBM-Schmidt")
        params_to_ket = [&](const dvec& params) { return RBMState::From(params, SCHMIDT, 3*nn_spins); };
    if (MODEL == "FF" || MODEL == "FF-Schmidt")
        params_to_ket = [&](const dvec& params) { return state.From(params, SCHMIDT); };

    // run problem
    std::ostream* out = &std::cout;
    std::ofstream fout;
    if (options.count("file") > 0) {
        fout.open(options["file"].as<std::string>().c_str(), std::ios::out);
        out = &fout;
    }

    run(problem, params_to_ket, SPINS, GENERATIONS, THREADS, POPULATION, VERBOSITY, ERROR_THRESHOLD, EVAL_THRESHOLD, OUT_PRECISION, OUT_THRESHOLD, LALG, *out);

    return 0;
}
