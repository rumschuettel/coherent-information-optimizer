#!/bin/bash
patch packages/include/pagmo/io.hpp pagmo.patch.1 

conda create -n dev-coherent --yes
source activate dev-coherent
conda install -c conda-forge nlopt --yes
conda install -c conda-forge boost --yes

mkdir -p build/debug
mkdir -p build/release

cd build/debug
cmake ../.. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=1
cd ../release
cmake ../.. -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=1
cd ../..
