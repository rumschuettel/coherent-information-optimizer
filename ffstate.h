#pragma once

#include "nn.h"

class FFState {
    private:
        const std::vector<Layer> layers;
        const size_t spins;

    public:
        FFState(const std::vector<Layer> &layers);

        scalar Weight(const rvec& in, const std::vector<double>& parameters) const;
        ket From(const std::vector<double>& parameters, const bool as_schmidt=false) const;

        size_t ParameterCount() const;
        size_t Spins() const;
};
