// taken from https://stackoverflow.com/questions/23688447/inserting-text-before-each-line-using-stdostream

#pragma once

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/array.hpp>
#include <cstring>
#include <limits>

#include <map>
#include <thread>
#include <mutex>

namespace print_multithreaded {
    extern std::mutex print_mutex;
    extern std::map<std::thread::id, size_t> lookup;
    extern std::thread::id last_id;
    extern size_t id_incr;

    // line_num_filter is a model of the Boost concept OutputFilter which
    // inserts a sequential line number at the beginning of every line.
    class line_num_filter : public boost::iostreams::output_filter
    {
        public:
            inline line_num_filter();

            template<typename Sink>
            inline bool put(Sink& snk, char c);

            template<typename Device>
            inline void close(Device&);

        private:
            bool m_start_of_line;
            unsigned int m_line_num;
            boost::array<char, 20> m_buf;
            const char* m_buf_pos;
            const char* m_buf_end;
    };

    inline line_num_filter::line_num_filter() :
        m_start_of_line(true),
        m_line_num(1),
        m_buf_pos(m_buf.data()),
        m_buf_end(m_buf_pos)
    {}

    // put() must return true if c was written to dest, or false if not.
    // After returning false, put() with the same c might be tried again later.
    template<typename Sink>
    inline bool line_num_filter::put(Sink& dest, char c)
    {
        // get thread id
        auto id = std::this_thread::get_id();

        if (lookup.count(id) == 0)
            lookup[id] = id_incr ++;
        
        if (last_id != id)
            last_id = id;
     
        // If at the start of a line, print thread id
        if (m_start_of_line) {
            m_buf_pos = m_buf.data();
            m_buf_end = m_buf_pos + std::snprintf(m_buf.data(), m_buf.size(), "\e[0;93mT%03lu\e[0m", lookup[id]);
            m_start_of_line = false;
        }

        // If there are buffer characters to be written, write them.
        // This can be interrupted and resumed if the sink is not accepting
        // input, which is why the buffer and pointers need to be members.
        while (m_buf_pos != m_buf_end) {
            if (!boost::iostreams::put(dest, *m_buf_pos))
                return false;
            ++m_buf_pos;
        }

        // Copy the actual character of data.
        if (!boost::iostreams::put(dest, c))
            return false;

        // If the character copied was a newline, get ready for the next line.
        if (c == '\n') {
            ++m_line_num;
            m_start_of_line = true;
        }
        return true;
    }

    // Reset the filter object.
    template<typename Device>
    inline void line_num_filter::close(Device&)
    {
        m_start_of_line = true;
        m_line_num = 1;
        m_buf_pos = m_buf_end = m_buf.data();
    }
} // end of namespace print_multithreaded

