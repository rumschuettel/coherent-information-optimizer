#include "tests.h"
#include "benchmark.h"
#include "quantum.h"
#include "rbmstate.h"
#include "ffstate.h"

#include "qpp/include/qpp.h"
#include <unsupported/Eigen/KroneckerProduct>

using Eigen::kroneckerProduct;

#define Complex scalar

namespace {
}

#include <iostream>
void tests()
{
    using std::cout;
    using std::endl;
    using qpp::disp;

    std::ios::fmtflags old_flags( cout.flags() );
	
	// dephrasure channel tests
	const ket dep_teststate_1 = (ket(64) << Complex(0.,0.),Complex(6.9831e-274,-6.778e-274),Complex(1.1176e-155,-1.9648e-155),Complex(-1.5862e-90,5.7593e-90),Complex(-1.2026e-257,-3.4909e-258),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(8.8974e-97,-3.138e-97),Complex(-1.2637e-53,-7.7058e-53),Complex(0.,0.),Complex(0.,0.),Complex(-5.537e-71,1.3449e-70),Complex(-2.4773e-102,2.8839e-100),Complex(1.3025e-111,1.602e-111),Complex(7.4423e-60,3.0346e-59),Complex(0.,0.),Complex(-4.703e-296,-3.8816e-296),Complex(0.,0.),Complex(-1.0807e-314,1.2252e-315),Complex(1.9102e-144,-2.2577e-143),Complex(-9.4776e-157,2.194e-156),Complex(0.,0.),Complex(0.,0.),Complex(1.4769e-93,1.1778e-93),Complex(-3.6841e-108,3.3137e-109),Complex(2.0268e-109,-2.5547e-109),Complex(3.3154e-140,-6.6645e-140),Complex(2.815e-152,-2.1777e-152),Complex(8.9791e-206,-8.5112e-207),Complex(-7.6202e-98,9.2743e-98),Complex(1.3504e-172,5.1684e-173),Complex(1.2645e-202,-4.4547e-203),Complex(0.,0.),Complex(-5.2059e-155,-1.8463e-154),Complex(0.,0.),Complex(1.8716e-83,3.3663e-84),Complex(-4.5943e-86,1.9566e-86),Complex(7.2838e-129,2.6887e-129),Complex(-2.4564e-210,-2.9693e-210),Complex(0.,0.),Complex(0.,0.),Complex(2.3206e-115,1.7871e-115),Complex(-1.8222e-92,-2.0264e-92),Complex(0.,0.),Complex(-5.4441e-233,-7.3479e-234),Complex(0.,0.),Complex(1.5e-323,0.),Complex(0.23572,0.97182),Complex(1.8716e-83,3.3663e-84),Complex(3.7176e-55,1.5175e-54),Complex(3.4341e-122,-7.7089e-122),Complex(-2.1018e-246,-1.7877e-246),Complex(3.1444e-110,2.8959e-110),Complex(-4.9738e-8,-8.2904e-8),Complex(1.4503e-90,2.1508e-91),Complex(0.,0.),Complex(0.,0.),Complex(1.9808e-161,-2.8619e-161),Complex(0.,0.),Complex(1.7448e-210,-1.8954e-211),Complex(1.1641e-313,-7.1982e-313),Complex(-5.e-324,0.),Complex(0.,0.)).finished();
	const ket dep_teststate_2 = (ket(64) << Complex(-0.0028712,-0.0047213),Complex(-4.17e-15,2.7193e-14),Complex(-4.1862e-132,-3.7894e-132),Complex(-0.53576,-0.78206),Complex(-3.0735e-252,4.294e-253),Complex(6.9995e-58,-1.4727e-58),Complex(-0.0090081,-0.0011304),Complex(-1.9876e-168,7.5848e-168),Complex(-1.4753e-46,-6.5004e-47),Complex(-2.0128e-37,-4.4892e-37),Complex(1.0648e-34,-8.876e-35),Complex(4.4859e-70,-4.9307e-70),Complex(-2.2396e-35,-1.484e-34),Complex(-3.3098e-43,-4.098e-42),Complex(1.5026e-63,8.7073e-64),Complex(-4.1931e-45,-6.8663e-45),Complex(5.5e-236,-2.0392e-235),Complex(1.86e-91,2.5385e-91),Complex(-0.0028976,-0.010521),Complex(-4.4311e-184,-7.1054e-184),Complex(-1.2991e-87,-8.4559e-86),Complex(2.044e-62,6.9009e-63),Complex(1.4759e-44,1.6474e-44),Complex(-4.5868e-53,-7.6549e-54),Complex(-2.1642e-87,-1.5612e-87),Complex(6.3502e-144,6.0969e-144),Complex(-3.5523e-90,2.3934e-90),Complex(-1.2092e-78,6.9683e-79),Complex(-1.8959e-39,4.8322e-39),Complex(-5.42e-155,4.7853e-155),Complex(9.917e-63,-4.8655e-63),Complex(-1.547e-21,-1.8044e-21),Complex(-2.6773e-51,-3.3973e-51),Complex(-7.4078e-80,-8.0022e-80),Complex(-3.3979e-17,-4.9498e-17),Complex(-6.2585e-49,3.8872e-48),Complex(-1.4896e-73,-1.0474e-74),Complex(-6.0538e-69,9.3032e-69),Complex(-3.252e-241,-7.544e-241),Complex(-1.4354e-101,-4.4134e-102),Complex(-4.1393e-27,2.2663e-27),Complex(-7.1481e-23,2.7499e-23),Complex(9.2991e-78,-2.147e-78),Complex(-2.6402e-72,1.184e-72),Complex(1.2592e-240,-4.7132e-239),Complex(-1.5573e-20,3.5577e-22),Complex(-1.9739e-79,6.0616e-79),Complex(-7.2291e-181,-1.0135e-180),Complex(-4.2399e-55,-7.0778e-56),Complex(2.7266e-58,-4.5245e-57),Complex(-0.000037024,0.000024749),Complex(-2.3833e-57,-4.724e-58),Complex(-5.4723e-44,-1.4171e-44),Complex(4.5144e-24,-2.1293e-24),Complex(-1.9964e-49,-6.9122e-49),Complex(-3.571e-52,2.6425e-51),Complex(-0.031619,0.10373),Complex(-0.016719,0.0090873),Complex(-1.6469e-51,-2.2269e-51),Complex(3.53e-220,6.6603e-221),Complex(-0.23757,-0.17709),Complex(2.5808e-64,6.3044e-64),Complex(0.020268,0.027914),Complex(6.6921e-22,-1.4614e-21)).finished();
	const ket dep_teststate_3 = (ket(64) << Complex(0.,0.),Complex(0.,0.),Complex(-0.49789,-0.0459),Complex(-4.0835e-265,-9.0498e-265),Complex(-2.1221e-15,-1.5555e-15),Complex(-3.8381e-198,9.2168e-198),Complex(0.,0.),Complex(2.3856e-296,-2.3856e-296),Complex(5.1028e-55,-7.5757e-55),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(-4.147e-61,2.6643e-61),Complex(-3.0982e-147,1.2911e-146),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(9.759e-231,1.5919e-230),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(-0.49789,-0.0459),Complex(0.,0.),Complex(-0.49789,-0.0459),Complex(0.,0.),Complex(0.,0.),Complex(-3.8779e-187,8.7566e-188),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(-5.0936e-153,-3.5368e-153),Complex(1.0228e-123,-3.6123e-124),Complex(0.,0.),Complex(-1.531e-196,7.4751e-196),Complex(-0.49789,-0.0459),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(1.8777e-279,-1.6276e-279),Complex(0.,0.),Complex(1.6357e-92,2.3567e-92),Complex(0.,0.),Complex(0.,0.),Complex(4.9135e-128,-1.7899e-127),Complex(0.,0.),Complex(-3.7599e-237,1.004e-235),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(5.5726e-68,-9.915e-68),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.)).finished();
	const ket dep_teststate_4 = (ket(64) << Complex(-2.3014e-153,-2.4408e-153),Complex(3.1019e-105,6.9333e-105),Complex(-8.1251e-36,-5.1926e-36),Complex(4.0501e-142,1.8925e-141),Complex(0.,0.),Complex(0.,0.),Complex(-0.94141,0.33726),Complex(3.0276e-285,-5.9965e-285),Complex(0.,0.),Complex(-4.1421e-254,2.1118e-254),Complex(-7.4562e-254,-2.8975e-254),Complex(6.1894e-71,4.636e-71),Complex(0.,0.),Complex(2.1668e-74,-1.1299e-75),Complex(0.,0.),Complex(2.147e-212,-2.2713e-212),Complex(4.9861e-261,1.0015e-260),Complex(0.,0.),Complex(0.,0.),Complex(0.,0.),Complex(-2.9246e-239,1.0121e-239),Complex(5.2871e-155,1.2535e-154),Complex(0.,0.),Complex(1.6475e-286,1.3109e-286),Complex(4.8241e-176,4.771e-176),Complex(-8.0538e-176,2.4718e-175),Complex(1.9017e-133,-1.5621e-133),Complex(9.5427e-161,-9.8027e-161),Complex(-3.0082e-66,3.0158e-66),Complex(0.,0.),Complex(-4.5701e-28,1.4369e-28),Complex(0.,0.),Complex(0.,0.),Complex(3.9436e-319,1.7949e-319),Complex(0.,0.),Complex(0.,0.),Complex(1.5784e-315,-6.1575e-316),Complex(2.5515e-41,6.4527e-42),Complex(-2.5553e-169,-1.6898e-168),Complex(0.,0.),Complex(-5.0394e-14,4.9678e-14),Complex(9.0048e-275,3.4008e-274),Complex(0.,0.),Complex(0.,0.),Complex(4.1072e-221,-2.4311e-220),Complex(0.,0.),Complex(-2.0405e-317,7.7776e-318),Complex(4.985e-296,-1.2502e-296),Complex(0.,0.),Complex(4.8195e-248,-1.0863e-247),Complex(-2.2924e-164,-7.3925e-165),Complex(-1.3422e-286,8.2457e-287),Complex(0.,0.),Complex(-7.7678e-106,-1.056e-105),Complex(3.3143e-253,7.1819e-254),Complex(-1.5411e-7,3.3195e-8),Complex(0.,0.),Complex(0.,0.),Complex(-1.3307e-145,2.8032e-145),Complex(4.2252e-238,8.0037e-238),Complex(0.,0.),Complex(-9.0482e-206,4.5527e-205),Complex(0.,0.),Complex(1.5513e-300,-2.3835e-300)).finished();
	const ket dep_teststate_5 = (ket(64) << Complex(-0.0054765,-0.043779),Complex(-9.4842e-243,-3.0216e-242),Complex(1.7812e-116,-2.4686e-118),Complex(-1.2402e-119,-3.2129e-120),Complex(-3.2264e-221,2.2636e-221),Complex(-4.9569e-164,-2.0077e-164),Complex(2.4122e-201,-5.0584e-201),Complex(4.2332e-46,-2.6017e-45),Complex(-4.5205e-125,-6.7907e-126),Complex(4.1401e-17,1.6354e-16),Complex(-2.2671e-86,3.4469e-86),Complex(-0.37411,0.43626),Complex(0.003209,-0.034505),Complex(7.2135e-96,-8.0827e-96),Complex(-3.3154e-22,1.3341e-22),Complex(0.,0.),Complex(-0.039207,-0.061001),Complex(-6.7695e-290,-5.9937e-290),Complex(-0.37411,0.43626),Complex(-5.328e-233,-2.4743e-233),Complex(-3.8686e-114,-1.2818e-115),Complex(1.3709e-53,5.0489e-53),Complex(-7.1013e-96,-1.9201e-94),Complex(0.0080558,-0.026456),Complex(3.1255e-14,-2.2492e-14),Complex(0.,0.),Complex(3.9187e-299,2.1993e-299),Complex(3.0148e-64,-2.927e-64),Complex(-1.2593e-121,6.7139e-123),Complex(7.6078e-155,1.2025e-155),Complex(-1.8359e-116,-7.7042e-117),Complex(1.109e-230,-1.9362e-230),Complex(-6.0467e-117,-8.9502e-118),Complex(1.764e-144,4.5531e-145),Complex(0.,0.),Complex(9.7479e-116,-1.0035e-115),Complex(0.,0.),Complex(7.5144e-7,-1.5682e-6),Complex(-2.3335e-136,3.1464e-135),Complex(-9.0098e-228,-7.6951e-228),Complex(6.2086e-20,3.4847e-20),Complex(2.4232e-73,5.7026e-73),Complex(-2.1583e-9,-5.2436e-10),Complex(-1.0263e-210,1.3792e-210),Complex(0.000010489,-0.000012632),Complex(-5.5392e-183,-2.2636e-183),Complex(1.5948e-32,-5.1907e-32),Complex(4.2652e-42,3.7341e-42),Complex(0.,0.),Complex(9.1817e-149,7.4476e-149),Complex(0.,0.),Complex(-3.2534e-8,-1.4652e-7),Complex(2.0836e-241,2.4702e-241),Complex(-6.6043e-16,1.659e-15),Complex(7.2317e-39,6.1865e-39),Complex(3.8019e-34,-3.6194e-34),Complex(-3.2534e-8,-1.4652e-7),Complex(-2.5966e-117,-4.7542e-117),Complex(-1.6993e-113,4.4643e-114),Complex(0.,0.),Complex(0.,0.),Complex(1.1804e-290,-1.8144e-290),Complex(2.1679e-179,1.6978e-179),Complex(-0.37411,0.43626)).finished();
	
	auto C_single = dephrasure_channel(0.116, 0.348);
    auto C = channel_and_env(C_single, 3, 3);
	auto sigma = ket2dm(dep_teststate_3);
	auto rho_AB = apply_super(C, sigma);
	auto rho_A = qpp::ptrace(rho_AB, {1}, { ipowk(3,3), ipow2(3) });
	cout << "dephrasure channel with p=.116, q=.348 on 3 qubits applied to test state" << endl;
	cout << "(" << entropy(rho_A) << " - " << entropy(rho_AB) << ")/3 = " << coherent_information(C, sigma, 3, 3, 3, 2)/3 << endl << endl;

    // a list of test cases
    cout << "depolarizing channel with p = .1" << endl;
    cout << disp( cmat(kraus2super(qubit_depolarizing_channel(.1))) ) << endl << endl;

    auto C1 = kraus2super(tensor(qubit_depolarizing_channel(.1), 2));
    cout << "depolarizing channel with p = .1, 2 copies" << endl;
    cout << disp(cmat(C1)) << endl << endl;

    cout << "apply 2 qubit depolarizing channel with p = .1 to |01>" << endl;
    cout << disp( apply_super(C1, ket2dm(kroneckerProduct(Ket0, Ket1))) ) << endl << endl;

    auto C2 = kraus2super(tensor(qubit_depolarizing_channel(.99), identity_channel(2, 1)));
    cout << "apply qubit depolarizing channel with p = .99 to first subsystem of |0><0| x |-><-|" << endl;
    cout << disp( apply_super(C2, ket2dm(kroneckerProduct(Ket0, KetM))) ) << endl << endl;

    cout << "conditional entropy with base 2 of |0><0| x 1" << endl;
    cout << conditional_entropy(qpp::kron(Rho00, ID), {1}, {2, 2}) << endl << endl;

    cout << "coherent information for two copies of qubit depolarizing channel over 1000 random states" << endl;
    cout << std::fixed << std::setprecision(3);
    for (double p = .2; p < .3; p += .005) {
		auto C_single = qubit_depolarizing_channel(p);
        auto C = channel_and_env(C_single, 2, 2);
        cout << measure<std::chrono::microseconds>::execution( [&C, p]() {
                cout << "p=" << p << ":\t" << coherent_information(C, 1000, 2, 2)/2 << " [time ";
                })/100 << "µs/call]" << endl;
    }
    cout << endl;

    cout << "coherent information for 100 random states, 3 channel uses" << endl;
    for (double p = .25; p < .26; p += .001) {
		auto C_single = qubit_depolarizing_channel(p);
        auto C = channel_and_env(C_single, 3, 3);
        cout << measure<std::chrono::microseconds>::execution( [&C, p]() {
                cout << "p=" << p << ":\t" << coherent_information(C, 100, 3, 3)/3 << " [time ";
                })/100 << "µs/call]" << endl;
    }
    cout << endl;

    cout << "coherent information for repetition code (Krauss channel application)" << "\np";
    for (size_t n = 1; n <= 7; n ++)
        cout << "\t\tn=" << n;
    cout << "\n";
    for (double p = .25; p < .255; p += 0.00001) {
        auto C = qubit_depolarizing_channel(p);

        cout << std::setprecision(5) << std::fixed << std::noshowpos  << p << "\t";
        cout << std::setprecision(4) << std::scientific << std::showpos;
        for (size_t n = 1; n <= 7; n ++) {
            auto rho = ket2dm(repetition_encode(KetP, n+1));
            cout << "\t" << coherent_information(C, rho, n)/n;
        }
        cout << "\n" << std::flush;
    }
    cout << "\n";
    cout.flags(old_flags);

    auto CC = kraus2super(tensor(identity_channel(2, 3), tensor(qubit_depolarizing_channel(.1), 3)));
    cout << "superoperator of size " << CC.rows() << "x" << CC.cols() << endl;

    // rbm state
    ket a(2), b(2);
    a << 2.*J, .3;
    b << 5., -1.*J+.2;
    cmat W(2, 2);
    W << 3., 2., 1., 0.;

    cout << "RBM State s with a=" << endl;
    cout << disp(a) << endl << "b=" << endl << disp(b) << endl << "W=" << endl << disp(W) << endl;
    cout << "s (Schmidt) =" << endl << disp(RBMState::From(a, b, W, true).normalized()) << endl << endl;

    // ffstate
    cout << "ffstate with 2->3 (ReLU)->2 (ReLU)" << endl;
    FFState state({
        Layer{2, 3, ReLU},
        Layer{3, 2, ReLU}
    });

    cout << "parameter count=" << state.ParameterCount() << endl;
    auto params = {.1, .3, .4, -.2, -.1, 1., .2, 10., 9., 8., .7, -.6, .5, .4, -.3, 2., 1.};
    cout << "s=" << disp(state.From(params)) << endl << endl;
    cout << "s (Schmidt)=" << disp(state.From(params, true)) << endl << endl;

}
